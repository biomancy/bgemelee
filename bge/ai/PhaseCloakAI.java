package bge.ai;

import java.util.List;

import org.lwjgl.util.vector.Vector2f;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.Iterator;
import org.lazywizard.lazylib.combat.AIUtils;

public class PhaseCloakAI implements ShipSystemAIScript {

	private ShipAPI ship;
	private CombatEngineAPI engine;
	private ShipwideAIFlags flags;
	private ShipSystemAPI system;
        private float strafeDistance;
	
	private final IntervalUtil tracker = new IntervalUtil(0.2f, 0.3f);
        
    private float bestStrafeDistance(ShipAPI ship){
        List<WeaponAPI> weapons = ship.getAllWeapons();
        float bestRange = 0f;
        for(Iterator iterMe = weapons.iterator(); iterMe.hasNext(); )
        {
            WeaponAPI weapon = (WeaponAPI) iterMe.next();
            if(weapon.getSpec().getType() != WeaponType.DECORATIVE && weapon.getSpec().getType() != WeaponType.LAUNCH_BAY){
                float weaponRange = weapon.getRange();
                if (weaponRange > bestRange){
                    bestRange = weaponRange;
                }
            }
        }
        if(bestRange > 0f){
            return Math.min(bestRange,1900f);
        } else {
            //Safety catch-all for suckage
            return 800f;
        }
    }        
	
    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
            this.ship = ship;
            this.flags = flags;
            this.engine = engine;
            this.system = system;
    }
	
    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if(engine.isPaused()) return;
        tracker.advance(amount);
        if (tracker.intervalElapsed()) {
            ship.resetDefaultAI();
            }			
        }
    }

