package bge.ai;

public final class AIData {
    private static AIData instance = null;

    public static int battleType = 0;
   
    AIData() 
    {
        // Exists only to defeat instantiation.
    }
    
   public static AIData getInstance() {
      if(instance == null) {
         instance = new AIData();
      } 
      return instance;
   }
}
