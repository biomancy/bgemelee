//Bitbucket update.

package bge.data.scripts;

import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import bge.data.scripts.world.himmel.BGEGen;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.combat.ShipAIPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import java.util.List;

import bge.ai.BGEMeleeAI;
import bge.data.shipsystems.scripts.ai.AdrenalineToggleAI; //custom AdrenalineToggle AI routine
import bge.data.shipsystems.scripts.ai.AlwaysOnAI;
import bge.data.shipsystems.scripts.ai.IntegrityFieldAI;
import bge.data.shipsystems.scripts.ai.OnOnceAI;
import bge.data.shipsystems.scripts.ai.HighEnergyFocusAI;
import java.util.Iterator;


//blah



public class BGEModPlugin extends BaseModPlugin
{
    private static void initBGE()
    {
        try
        {
            Global.getSettings().getScriptClassLoader().loadClass("data.scripts.world.ExerelinGen");
            //Exerelin running, so only load Exerelin generators
        }
        
        catch (ClassNotFoundException ex)
        {
            new BGEGen().generate(Global.getSector());
            // Exerelin not found so continue and run normal generation code
        }
    }

    @Override
    public void onNewGame()
    {
        initBGE();
        Global.getLogger(BGEModPlugin.class).log(org.apache.log4j.Level.INFO,"Initialised BGE");
        
    }
    
    
@Override
    @SuppressWarnings("unchecked")
    public PluginPick<ShipAIPlugin> pickShipAI(FleetMemberAPI member, ShipAPI ship) 
    {
        List weapons = ship.getAllWeapons();
        if (weaponCheck(weapons))
        {            
                Global.getLogger(BGEModPlugin.class).log(org.apache.log4j.Level.INFO,"Picked Brawler");
                return new PluginPick(new BGEMeleeAI(ship, pickSystemAIScript(ship), pickCloakAIScript(ship), true, 1), CampaignPlugin.PickPriority.MOD_SPECIFIC);
        }  
        else
        {
            return null;
        }
    }
    
            private static boolean weaponCheck(List weapons)
        {
            boolean returnBoolean = false;
        for (Iterator it = weapons.iterator(); it.hasNext();) {
            Object weaponObject = it.next();
            WeaponAPI weaponIDCheck = (WeaponAPI) weaponObject;
            Global.getLogger(BGEModPlugin.class).log(org.apache.log4j.Level.INFO,weaponIDCheck.getId());
            if (weaponIDCheck.getId().contentEquals("lrg_scythe_right") 
                    || weaponIDCheck.getId().contentEquals("lrg_scythe_left")
                    || weaponIDCheck.getId().contentEquals("med_scythe_left")
                    || weaponIDCheck.getId().contentEquals("med_scythe_right")
                    || weaponIDCheck.getId().contentEquals("lrg_pincer_right")
                    || weaponIDCheck.getId().contentEquals("lrg_pincer_left")
                    || weaponIDCheck.getId().contentEquals("med_pincer_right")
                    || weaponIDCheck.getId().contentEquals("med_pincer_right")                   
                    || weaponIDCheck.getId().contentEquals("bge_mord_grappler")                
                    || weaponIDCheck.getId().contentEquals("bge_mini_grappler"))
            {
                Global.getLogger(BGEModPlugin.class).log(org.apache.log4j.Level.INFO,"weaponCheck TRUE");
                returnBoolean = true;
                break;
            }
            else
            {
                Global.getLogger(BGEModPlugin.class).log(org.apache.log4j.Level.INFO,"weaponCheck FALSE");
                returnBoolean = false;
            }
        }
            return returnBoolean;
        }
            
    private static ShipSystemAIScript pickSystemAIScript(ShipAPI ship)
        {
            if(ship.getSystem() == null)
            {
                return null;
            } else if (ship.getSystem().getId().contains("fungal_spores") || 
                    ship.getSystem().getId().contains("mord_drone_hyperspace_web") ||
                    ship.getSystem().getId().contains("stem_cell_cure"))
            {
                return new AlwaysOnAI();
            } else if (ship.getSystem().getId().contains("flux_parasites") ||
                    ship.getSystem().getId().contains("female_pheremones"))
            {
                return new IntegrityFieldAI();
            } else if (ship.getSystem().getId().contains("brut_drone") ||
                    ship.getSystem().getId().contains("mord_drone"))
            {
                return new OnOnceAI();
            } else if (ship.getSystem().getId().contains("berzerk"))
            {
                return new HighEnergyFocusAI();
            } else
            {
                return new AlwaysOnAI();
            }
        }
            
    private static ShipSystemAIScript pickCloakAIScript(ShipAPI ship){
        Global.getLogger(BGEModPlugin.class).log(org.apache.log4j.Level.INFO,ship.getPhaseCloak().getId());
        if(ship.getPhaseCloak() == null)
        {
            return null;
            
        } else if (ship.getPhaseCloak().getId().contains("adrenaline_rush"))
        {
            return new AdrenalineToggleAI();
            
        } else
        {            
            return new bge.ai.PhaseCloakAI();
        }
    } 
        
    private static boolean shouldStrafe(ShipAPI ship){
//        String systemId = ship.getSystem().getId();
//        //Special exception for Burn Drive here, since attempting to strafe with it is counter-productive
//        if(systemId.contains("burndrive")) return false;
//        if(!ship.isFighter()) return true;//We can change this later
//        return ship.getWing().getRole() !=
//                WingRole.BOMBER &&
//                ship.getWing().getRole() !=
//                WingRole.INTERCEPTOR;
            return true;
    }
    
    private static float bestStrafeDistance(ShipAPI ship){
        List weapons = ship.getAllWeapons();
        float bestRange = 0f;
        for (Iterator it = weapons.iterator(); it.hasNext();) {
            WeaponAPI weapon = (WeaponAPI)it.next();
            if(weapon.getSpec().getType() != WeaponAPI.WeaponType.DECORATIVE && weapon.getSpec().getType() != WeaponAPI.WeaponType.LAUNCH_BAY){
                float weaponRange = weapon.getRange();
                /* if (weaponRange > bestRange){
                bestRange = weaponRange;
                }*/
            }
        }
        if(bestRange > 0f){
            return Math.min(bestRange - 150f,1900f);
        } else {
            //Safety catch-all for suckage
            return 80f;
        }
    }
    
}
