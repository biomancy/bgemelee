package bge.data.scripts.world;


import com.fs.starfarer.api.Global;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;

import com.fs.starfarer.api.campaign.FleetDataAPI;

import com.fs.starfarer.api.fleet.FleetMemberAPI;

import com.fs.starfarer.api.fleet.FleetMemberType;

import java.awt.Color;
import java.util.*;



public final class BGEVariantSelector

{
    
private static final boolean SHOW_DEBUG = false;
    
private static final boolean SHOW_ERRORS = false;
    
// This is recalculated every time you load the game, so there is no need
    
// to create a new save every time you add a variant to the mod
    

public static final Map allVariants;
    
private static final Random rng = new Random();

    
static 
{
        
Map allVariantsTemp = new HashMap();    
        
allVariantsTemp.put("bge_waechter",createVariantList(new String[]{"bge_waechter_standard", "bge_waechter_siege"}));
        
allVariantsTemp.put("bge_koenig",createVariantList(new String[]{"bge_koenig_standard", "bge_koenig_siege"}));
allVariantsTemp.put("bge_zophe",createVariantList(new String[]{"bge_zophe_standard", "bge_zophe_siege"}));
allVariantsTemp.put("bge_flusskrebs",createVariantList(new String[]{"bge_flusskrebs_standard", "bge_flusskrebs_siege"}));
allVariantsTemp.put("bge_arbiter",createVariantList(new String[]{"bge_arbiter_standard", "bge_arbiter_siege"}));
allVariantsTemp.put("bge_vergifter",createVariantList(new String[]{"bge_vergifter_standard", }));
allVariantsTemp.put("bge_krieger",createVariantList(new String[]{"bge_krieger_standard", }));


allVariants = Collections.unmodifiableMap(allVariantsTemp);
    
}

    

private BGEVariantSelector()
    

{
        
// VariantSelector can't be instantiated nor subclassed
    
}

    
// Minor convenience tweak for better readability in the static initializer
    
private static List createVariantList(String[] variants)
    
{
        
return Collections.unmodifiableList(Arrays.asList(variants));
    
}

    
public static String getRandomVariant(String hullType)
    
{
        
// Check if we have any variants registered for this hull type
        
if (!allVariants.keySet().contains(hullType))
        
{
            
// If there are no variants, the ship remains a hull
            
showError("No variants found for '" + hullType + "'!");
            
return hullType + "_Hull";
        
}

        

// Choose randomly from the registered variants
        
List tmp = (List) allVariants.get(hullType);
        
return (String) tmp.get(rng.nextInt(tmp.size()));
    
}

    
public static void randomizeFleet(CampaignFleetAPI fleet)
    
{
        
FleetDataAPI data = fleet.getFleetData();
        
List members = data.getMembersListCopy();
        
Iterator iter = members.iterator();
        
FleetMemberAPI ship;

        
showDebug("Randomizing fleet '" + fleet.getFullName()
                + "'. Total ships: " + members.size() + ".");

        
// Iterate over the entire fleet, replace any hulls with a random variant
        

while (iter.hasNext())
        
{
            
ship = (FleetMemberAPI) iter.next();

            
if (ship.isFighterWing())
            
{
                
showDebug("Ignoring '" + ship.getSpecId() + "'.");
                
continue;
            
}

            
// If this is a hull, grab a random variant instead
            
if (ship.getSpecId().endsWith("_Hull"))
            
{
                
showDebug("Randomizing '" + ship.getSpecId() + "'.");
                
randomizeShip(ship, data);
            
}
        
}
    
}

    
private static void randomizeShip(FleetMemberAPI ship, FleetDataAPI data)
    
{
        if (ship == null || data == null)
        
{
            
showError("Ship or fleet data was null!");
            
return;
        
}

        
// Remove the variant from the ship id so we can look it up
        
String hullType = ship.getSpecId();
        
hullType = hullType.substring(0, hullType.lastIndexOf("_"));

        
// Check if we have any variants registered for this hull type
        
if (!allVariants.keySet().contains(hullType))
        
{
            
showError("No variants found for '" + hullType + "'!");
            
return;
        
}

        
// Get a random variant name from the master list
        
String variant = getRandomVariant(hullType);
        
FleetMemberAPI newShip;

        
// Try to create a ship
        
try
        
{
            
newShip = Global.getFactory().createFleetMember(
                    

FleetMemberType.SHIP, variant);
        
}
        

catch (Exception ex)
        
{
            

showError("Could not create variant '" + variant + "'!");
            
return;
        
}

        
// If we successfully created a ship, swap out the old one with the new
        
if (newShip != null)
        
{
            
showDebug("Swapping '" + ship.getSpecId() + "' for '"
                    + newShip.getSpecId() + "'.");

            
data.removeFleetMember(ship);
            
data.addFleetMember(newShip);
            
return;
        
}

        
showError("Ship was null!");
    
}

    
private static void showDebug(String msg)
    
{
        
if (SHOW_DEBUG)
        
{
            
Global.getSector().addMessage(msg);
        
}
    
}

    
private static void showError(String msg)
    
{
        
if (SHOW_ERRORS)
        
{
            
Global.getSector().addMessage(msg, Color.RED);
        
}
    }
}
