    package bge.data.shipsystems.scripts;
     
    import com.fs.starfarer.api.combat.MutableShipStatsAPI;
    import com.fs.starfarer.api.combat.ShipAPI;
    import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
     
    //import data.scripts.plugins.AdrenalineTracker;
    import data.scripts.plugins.AdrenalineToggleTracker;
   
     
     
           
     
    public class adrenaline_toggle implements ShipSystemStatsScript {
       
        private boolean juiceMe = false;
     
            public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel)
            {
                if (state == state.IN)
                {
                    if (juiceMe == false) {
                        //AdrenalineTracker.startAdrenaline(4f,(ShipAPI)stats.getEntity());
                        AdrenalineToggleTracker.toggleAdrenaline((ShipAPI)stats.getEntity());
                        juiceMe = true;
                    }    
                   
                   
                }
                       
            }
           
            public void unapply(MutableShipStatsAPI stats, String id)
            {
                juiceMe = false;
     
            }
           
            public StatusData getStatusData(int index, State state, float effectLevel) {
                    return null;
     
            }
    }