package bge.data.shipsystems.scripts.ai;


import org.lwjgl.util.vector.Vector2f;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.FluxTrackerAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;

public class IntegrityFieldAI implements ShipSystemAIScript {

    private ShipAPI ship;
    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipSystemAPI system;

    private boolean shouldUseSystem = false;
    private final IntervalUtil tracker = new IntervalUtil(0.1f, 0.15f);
	
    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.flags = flags;
        this.engine = engine;
        this.system = system;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) 
    {
        tracker.advance(amount);
        if (tracker.intervalElapsed()) 
        {
            FluxTrackerAPI flux = ship.getFluxTracker();
            float level = flux.getFluxLevel();
            if (level > 0.9f)//We're probably heading towards overload, shut it down!
            {
                shouldUseSystem = false;
            } else {
                shouldUseSystem = true;
            }

            // If system is inactive and should be active, enable it
            // If system is active and shouldn't be, disable it
            if (ship.getSystem().isActive() == true && shouldUseSystem == false)
            {
                ship.useSystem();
            }			
            if(ship.getSystem().isActive() == false && shouldUseSystem == true)
            {
                ship.useSystem();
            }			
        }
    }
}
