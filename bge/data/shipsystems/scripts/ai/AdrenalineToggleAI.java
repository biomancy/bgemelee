/* Original code by Xenoargh
modified by Debido/Tecrys
*/	

    package bge.data.shipsystems.scripts.ai;
     
    import com.fs.starfarer.api.Global;
    import java.util.List;
     
    import org.lwjgl.util.vector.Vector2f;
     
    import com.fs.starfarer.api.combat.CombatEngineAPI;
    import com.fs.starfarer.api.combat.FluxTrackerAPI;
    import com.fs.starfarer.api.combat.ShipAPI;
    import com.fs.starfarer.api.combat.ShipCommand;
    import com.fs.starfarer.api.combat.ShipSystemAIScript;
    import com.fs.starfarer.api.combat.ShipSystemAPI;
    import com.fs.starfarer.api.combat.ShipwideAIFlags;
    import com.fs.starfarer.api.combat.WeaponAPI;
    import com.fs.starfarer.api.combat.WeaponAPI.WeaponType;
    import com.fs.starfarer.api.util.IntervalUtil;
    import data.scripts.plugins.AdrenalineToggleTracker;
    import java.util.Iterator;
    import org.lazywizard.lazylib.combat.AIUtils;
    import org.apache.log4j.Level;
     
    public class AdrenalineToggleAI implements ShipSystemAIScript {
     
	private static final float HULL_PERCENT = 0.8f;
        private static final float FLUX_PERCENT = 0.65f;
        private ShipAPI ship;
	private CombatEngineAPI engine;
	private ShipwideAIFlags flags;
	private ShipSystemAPI system;
        private float strafeDistance;
        
           
            private final IntervalUtil tracker = new IntervalUtil(0.2f, 0.3f);
           
        private float bestStrafeDistance(ShipAPI ship){
            List weapons = ship.getAllWeapons();
            float bestRange = 0f;
            for(Iterator iterMe = weapons.iterator(); iterMe.hasNext(); )
            {
                WeaponAPI weapon = (WeaponAPI) iterMe.next();
                if(weapon.getSpec().getType() != WeaponType.DECORATIVE && weapon.getSpec().getType() != WeaponType.LAUNCH_BAY){
                    float weaponRange = weapon.getRange();
                    if (weaponRange > bestRange){
                        bestRange = weaponRange;
                    }
                }
            }
            if(bestRange > 0f){
                return Math.min(bestRange,1900f);
            } else {
                //Safety catch-all for suckage
                return 800f;
            }
        }        
           
        @Override
        public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
                this.ship = ship;
                this.flags = flags;
                this.engine = engine;
                this.system = system;
                Global.getLogger(AdrenalineToggleAI.class).log(Level.INFO,"Initialising AdrenalineToggleAI");
        }
           
        @Override
            public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if(engine.isPaused()) return;
        tracker.advance(amount);
        if (tracker.intervalElapsed()) {
            FluxTrackerAPI fluxTracker = ship.getFluxTracker();
            strafeDistance = bestStrafeDistance(ship);
            
            boolean shouldUseSystem = false;
//            List nearbyEnemies = AIUtils.getNearbyEnemies(ship, 5000f);
//            List nearerEnemies = AIUtils.getNearbyEnemies(ship, strafeDistance -100f);
//            List nearbyMissiles = AIUtils.getNearbyEnemyMissiles(ship, 3000f);
            
            
            //shouldUseSystem = nearbyEnemies.isEmpty() || nearerEnemies.isEmpty() || nearbyMissiles.isEmpty();
           //Global.getLogger(AdrenalineToggleAI.class).log(Level.INFO,"Choosing to activate");
           
            			FluxTrackerAPI flux = ship.getFluxTracker();
			float level = flux.getFluxLevel();
                        float hplevel = ship.getHullLevel();
                        if (level > 0.8f || hplevel > 0.9f /*|| (ship.getSystem().isOn() && ship.getSystem().getId().contains("berzerk")*/ )//We're probably heading towards overload, start it up.
            {
            shouldUseSystem = false;
            }
            else
            {
            shouldUseSystem = true;
            }


//             If system is inactive and should be active, enable it
//             If system is active and shouldn't be, disable it
            if (AdrenalineToggleTracker.getSystem.isOff(ship))
            {
                if(shouldUseSystem) ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK,null,0);
                tracker.randomize();
            }else if (AdrenalineToggleTracker.getSystem.isOn(ship)){
                if(!shouldUseSystem) ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK,null,0);
                tracker.randomize();
            }			
        }
    }
}

