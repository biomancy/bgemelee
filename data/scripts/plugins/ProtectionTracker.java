	

    package data.scripts.plugins;
     
    import com.fs.starfarer.api.Global;
    import com.fs.starfarer.api.combat.CombatEngineAPI;
    import com.fs.starfarer.api.combat.EveryFrameCombatPlugin;
    import com.fs.starfarer.api.combat.ShipAPI;
    import java.util.Iterator;
    import java.util.List;
    import org.apache.log4j.Level;
     
    /* Original script by LazyWizard (I think)
    Modified (hit with a mallet) by Debido
    So what is this script? This script is a derivitive of the Incendiary script/
    It's purpose is to enable someone to activate the script, apply certain mutablestat effects to a ship
    After a given time the effects are un-applied to the ship. That is the high level.
     
    What happens at a psuedo-code level is this
     
    - The adrenaline_rush.java script is called when the player right-clicks or is activated by the AI.
    - The adrenaline_rush.java file has a single line that calls the startAdrenaline(float duration, shipAPI source) method in this class file
    - The startAdrenaline method does several things
    1. Applies the desired buffs/effects to the ship
    2. Creates a token in the protectionMap Hashmap
     
    - Conceptually a token is created but token has other connotations in Java so I'll clarify, but I will use the term token in the conceptual sense.
    - The 'token' created is a new ProtectionData object with the public ProtectionData(float protectionDuration, ShipAPI source) method
     
    - The important aspect of this token is that on creation it has a time value 'expiration' that is the current in-game time + protectionDuration in seconds that are passed to it from the shipsystem script startAdrenaline method..
    - Every frame of the game it will now check this script, iterate through the list of tokens in the protectionMap HashMap and determine if the current game time is equal to the time in the duration variable.
     
    - IF the game time is EQUAL to the time in the token, then it processes the token.
     
    - The processing of the token is thus:
    1. un-apply the mutable stats
    2. remove the token from the protectionMap hashmap
     
     
    Enjoy and don't have too much fun!
     
    */
     
     
    public class ProtectionTracker implements EveryFrameCombatPlugin
    {
        // Stores the currently protectionMap protectionTokens
        // Having the Set backed by a WeakHashMap helps prevent memory leaks
        //private static final Set protectionMap = Collections.newSetFromMap(new WeakHashMap());
        
  
        private CombatEngineAPI engine;
     
     
        @Override
        public void advance(float amount, List events)
        {
            if (engine.isPaused() || ProtectionData.protectionMap.isEmpty())
            {
                return;
            }
            // Deal protectionToken damage for all actively protectionMap ships
            for (Iterator iter = ProtectionData.protectionMap.iterator(); iter.hasNext();)
            {
                ProtectionData protectionToken = (ProtectionData) iter.next();
     
                // Check if the protectionToken has gone out
                if (engine.getTotalElapsedTime(false) >= protectionToken.expiration)
                {
                    ProtectionData.unApplyProtectionStats((ShipAPI)protectionToken.source.get());
                    //unApplyAdrenalineStats(protectionToken.source);
                    Global.getLogger(ProtectionTracker.class).log(Level.INFO,"Running protection un-apply loop");
                    iter.remove();
                    Global.getLogger(ProtectionTracker.class).log(Level.INFO,"Removed protection token");
                }
            }
        }
     
    
        @Override
        public void init(CombatEngineAPI engine)
        {
            this.engine = engine;
            ProtectionData.protectionMap.clear();
        }
     

    }

