	

    package data.scripts.plugins;
     
    import com.fs.starfarer.api.Global;
    import com.fs.starfarer.api.combat.CombatEngineAPI;
    import com.fs.starfarer.api.combat.EveryFrameCombatPlugin;
    import com.fs.starfarer.api.combat.FluxTrackerAPI;
    import com.fs.starfarer.api.combat.ShipAPI;
    import com.fs.starfarer.api.util.IntervalUtil;
    import java.lang.ref.WeakReference;
    import java.util.Collections;
    import java.util.Iterator;
    import java.util.List;
    import java.util.Set;
    import java.util.HashMap;
    import org.apache.log4j.Level;


    
    
     
     
     
    public class AdrenalineToggleTracker implements EveryFrameCombatPlugin
    {
        // Stores the currently juiced juiceTokens
        // Having the Set backed by a WeakHashMap helps prevent memory leaks
        //private static final Set juiced = Collections.newSetFromMap(new WeakHashMap());
        private static final Set juiced = Collections.newSetFromMap(new HashMap());
        private CombatEngineAPI engine; //probably don't need this anymore
        private final float INTERVAL_FRACTION = 0.2f;
        private final IntervalUtil tracker = new IntervalUtil(INTERVAL_FRACTION, INTERVAL_FRACTION);
        private final static float FLUX_BASE_MULTIPLIER = 0.3f;
        //private final float FLUX_PER_SECOND = 1000f*INTERVAL_FRACTION;
        

        @Override
        public void advance(float amount, List events)
        {
            if (engine.isPaused() || juiced.isEmpty())
            {
                return;
            }
            
            tracker.advance(amount);
            
            if (tracker.intervalElapsed())
            {
                for (Iterator iter = juiced.iterator(); iter.hasNext();)
                    {
                        AdrenalineData juiceToken = (AdrenalineData) iter.next();
                        ShipAPI ship = (ShipAPI)juiceToken.source.get();
                        FluxTrackerAPI fluxTracker = ship.getFluxTracker();
                        if (ship.getFluxTracker().isOverloadedOrVenting()) //if the ship is venting or overloading
                        {
                            unApplyAdrenalineStats(ship); //unapply stats buff
                            iter.remove(); //remove from list
                            continue; //go back to start of loop and re-evaluate.
                        }
                      
                        Global.getLogger(AdrenalineToggleTracker.class).log(Level.INFO,ship.getMutableStats().getPhaseCloakActivationCostBonus());
                        float flux_add = (ship.getMutableStats().getFluxDissipation().base) * INTERVAL_FRACTION * FLUX_BASE_MULTIPLIER;
                        fluxTracker.increaseFlux(flux_add, false);
                        
                    }
            }
            

        }
        
                @Override
        public void init(CombatEngineAPI engine)
        {
            this.engine = engine;
            juiced.clear();

        }
        
     
            private static void applyAdrenalineStats(ShipAPI source)
            {
                String id = "steroids"; //id of the buff
                Global.getLogger(AdrenalineToggleTracker.class).log(Level.INFO,"Running apply"); //debug code
               
                //Apply ship stats
                source.getMutableStats().getMaxCombatHullRepairFraction().modifyFlat(id, 100f);
                source.getMutableStats().getHullCombatRepairRatePercentPerSecond().modifyFlat(id, 3f);
                source.getMutableStats().getEmpDamageTakenMult().modifyPercent(id, - 60f);
                source.getMutableStats().getHighExplosiveDamageTakenMult().modifyPercent(id, - 50f);
                source.getMutableStats().getKineticDamageTakenMult().modifyPercent(id, - 40f);
                source.getMutableStats().getEnergyDamageTakenMult().modifyPercent(id, - 50f);
                source.getMutableStats().getFragmentationDamageTakenMult().modifyPercent(id, - 30f);
                source.getMutableStats().getCombatEngineRepairTimeMult().modifyMult(id, 0.6f);
                source.getMutableStats().getCombatWeaponRepairTimeMult().modifyMult(id, 0.4f);
                source.getMutableStats().getFluxDissipation().modifyPercent(id, 0f);
                source.getMutableStats().getAcceleration().modifyPercent(id,  60f);
                source.getMutableStats().getDeceleration().modifyPercent(id,  60f);
                source.getMutableStats().getTurnAcceleration().modifyPercent(id,  60f);
                source.getMutableStats().getMaxTurnRate().modifyPercent(id,  30f);
                source.getMutableStats().getMaxSpeed().modifyPercent(id,  30f);
                source.getMutableStats().getZeroFluxMinimumFluxLevel().modifyFlat(id,  1f);
       
            }
           
                    private static void unApplyAdrenalineStats(ShipAPI source)
            {
                String id = "steroids"; //id of the buff
                Global.getLogger(AdrenalineToggleTracker.class).log(Level.INFO,"Running un-apply"); //debug code
               
                //unpply ship stats            
                source.getMutableStats().getMaxCombatHullRepairFraction().unmodify(id);
                source.getMutableStats().getHullCombatRepairRatePercentPerSecond().unmodify(id);
                source.getMutableStats().getEmpDamageTakenMult().unmodify(id);
                source.getMutableStats().getHighExplosiveDamageTakenMult().unmodify(id);
                source.getMutableStats().getKineticDamageTakenMult().unmodify(id);
                source.getMutableStats().getEnergyDamageTakenMult().unmodify(id);
                source.getMutableStats().getFragmentationDamageTakenMult().unmodify(id);
                source.getMutableStats().getCombatEngineRepairTimeMult().unmodify(id);
                source.getMutableStats().getCombatWeaponRepairTimeMult().unmodify(id);
                source.getMutableStats().getFluxDissipation().unmodify(id);
                source.getMutableStats().getAcceleration().unmodify(id);
                source.getMutableStats().getDeceleration().unmodify(id);
                source.getMutableStats().getTurnAcceleration().unmodify(id);
                source.getMutableStats().getMaxTurnRate().unmodify(id);
                source.getMutableStats().getMaxSpeed().unmodify(id); 
                source.getMutableStats().getZeroFluxMinimumFluxLevel().unmodify(id);      
            }
       

     
     

     
        public static void startAdrenaline(ShipAPI source)
        {
            //Creates a new token in the juiced list
            juiced.add(new AdrenalineData(source));
            //Applies the mutablestats to the ship
            applyAdrenalineStats(source);
        }
        
        public static void stopAdrenaline(ShipAPI source)
        {
                for (Iterator iter = juiced.iterator(); iter.hasNext();)
                    {
                    AdrenalineData shipToken = (AdrenalineData) iter.next();
                        if (source == (ShipAPI)shipToken.source.get())
                        {
                            iter.remove();
                        } 

                    }
     
            unApplyAdrenalineStats(source);
        }
        
        public static void toggleAdrenaline(ShipAPI source)
        {
            if (getSystem.getState(source))
            {
                stopAdrenaline(source);
            }
            else
            {
                startAdrenaline(source);
            }
        }
     
      

     
        private static class AdrenalineData
        {
            public final WeakReference source;
            //public final ShipAPI source;
     
            public AdrenalineData(ShipAPI source)
            {
                this.source = new WeakReference(source);
            }        
        }
        
        public static class getSystem
        {
                //private boolean m_SystemState;
                //private ShipAPI m_Ship;
                public static boolean isOn(ShipAPI ship)
                {
                    //this.m_Ship = ship;
                    boolean l_returnState = false;
                    //get state of system, check if on, then return true

                    if (getState(ship)) l_returnState = true;

                    return l_returnState;
                }

                    public static boolean isOff(ShipAPI ship)
                {
                    //this.m_Ship = ship;
                    boolean l_returnState = true;
                    //get state of system, check if on, then return true

                    if (getState(ship)) l_returnState = false;

                    return l_returnState;
                }

                private static boolean getState(ShipAPI ship)
                {
                    boolean l_returnState = false;

                    for (Iterator iter = juiced.iterator(); iter.hasNext();)
                    {
                    AdrenalineData shipToken = (AdrenalineData) iter.next();
                        if (ship == (ShipAPI)shipToken.source.get())
                        {
                            l_returnState = true;
                        } 

                    }

                    return l_returnState;

            }
        }
        
    }