/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAPI;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import org.apache.log4j.Level;

/**
 *
 * @author Revan
 */
public class ProtectionData {
    
            public final WeakReference source;
            //public final ShipAPI source;
            final float expiration;
            static final Set protectionMap = Collections.newSetFromMap(new HashMap());
     
    public ProtectionData(float protectionDuration, ShipAPI source)
        {
            //this.hitLoc = new AnchoredEntity(target, hitLoc);
            this.source = new WeakReference(source);
            //this.source = source;
            //sets the time WHEN it will expire
            expiration = Global.getCombatEngine().getTotalElapsedTime(false)
                    + protectionDuration;
        } 
            
    public static void startProtection(float protectionDuration, ShipAPI source)
        {
            //Creates a new token in the protectionMap list
            protectionMap.add(new ProtectionData(protectionDuration, source));
            Global.getLogger(ProtectionTracker.class).log(Level.INFO,"Protection Requested");
            //Applies the mutablestats to the ship
            applyProtectionStats(source);
        }
                    
    static void applyProtectionStats(ShipAPI source)
        {
            String id = "faceMeltProtection"; //id of the buff
            Global.getLogger(ProtectionTracker.class).log(Level.INFO,"Running protection apply"); //debug code

            //Apply ship stats

        source.getMutableStats().getHighExplosiveDamageTakenMult().modifyMult(id,0.001f);

        }
    static void unApplyProtectionStats(ShipAPI source)
        {
            String id = "faceMeltProtection"; //id of the buff
            Global.getLogger(ProtectionTracker.class).log(Level.INFO,"Running protection un-apply"); //debug code

            //unpply ship stats            
            source.getMutableStats().getHighExplosiveDamageTakenMult().unmodify(id);

        }
 
}
